<?php
session_start();
include_once("controller/database.php");
?>
<!DOCTYPE html>
<!--
A file which shows current items in the user's basket.
-->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>View shopping cart</title>
        <link href="css/basket.css" rel="stylesheet" type="text/css"></head>
    <body>
        <h1>View Cart</h1>
        <div class="view-basket">
            <?php
            $current_url = "index.php";
            if (isset($_SESSION["displayBooks"])) {
                //echo '<pre>';
                //var_dump($_SESSION["displayBooks"]);
                //echo '</pre>';
                $total = 0;
                echo '<form method="get" action="controller/process_order.php">';
                echo '<ul>';
                $items = 0;
                $user_balance = $_SESSION['user_balance'];
                foreach ($_SESSION["displayBooks"] as $book) {
                    $book_isbn = $book["isbn"];

                    $conn = getDb();
                    $sql = $conn->prepare("SELECT book_title, price FROM book WHERE isbn=:product_code LIMIT 1");
                    $sql->bindValue(":product_code", $book_isbn);
                    $sql->execute();
                    $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
                    $rows = $sql->fetchAll();
                    foreach ($rows as $row) {
                        echo '<li class="basket">';
                        echo '<h3>' . $row['book_title'] . '</h3> ';
                        echo '<div class="isbn">ISBN :' . $book_isbn . '</div>';
                        echo '<div class="price">£' . $row['price'] . '</div>';
                        echo '<div class="qty">Qty : ' . $book["qty"] . '</div>';
                        echo '<span class="remove"><a href="update_basket.php?remove=' . $book["isbn"] . '&return_url=' . $current_url . '">&times; REMOVE</a></span>';
                        echo '</li>';
                        $subtotal = ($book["price"] * $book["qty"]);
                        $total = ($total + $subtotal);

                        echo '<input type="hidden" name="item_code[' . $items . ']" value="' . $book_isbn . '" />';
                        echo '<input type="hidden" name="item_qty[' . $items . ']" value="' . $book["qty"] . '" />';
                        $items ++;
                    }
                }
                echo '</ul>';

                echo '<strong class="total">Total : £' . $total . '</strong>  ';
                if (!($total > $user_balance)) {
                    echo '<div id="order">';
                    echo '<input type="submit" name="Order"></input>';
                    echo '</div>';
                } else {
                    echo '<div class="insufficient_funds">';
                    echo '<p>Your balance: £' . $user_balance . '</p>';
                    echo '<p>You do not have sufficient credit to make a purchase this large, please go instore to top up.</p>';
                    echo '</div>';
                }
                echo '</form>';
            } else {
                echo 'Your Cart is empty';
            }
            ?>
        </div>
    </body>
</html>

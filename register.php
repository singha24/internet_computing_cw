<?php
include '/DAO/RegistrationDAO.php';
if (isset($_REQUEST['register']) && $_REQUEST['register'] = "Submit") {
    register();
}
/*
 * Registration form
 */
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Register</title>
        <link rel="stylesheet" type="text/css" href="css/register.css">
        <script type="text/javascript">
            function validate() {
                var errors = "";
                var fail = false;

                var name = document.forms["new_user"]["forename"].value;
                var id = document.forms["new_user"]["student_id"].value;

                if (name == null || name == "") {
                    errors += "You must have a name..?\n";
                    fail = true;
                }

                var isbn_length = id.length;

                if (id == null || id == "") {
                    errors += "Please provide you student id.\n";
                    fail = true;
                }

                if (fail) {
                    alert(errors);
                }

                return true;
            }
        </script>
    </head>
    <body>
        <h1> Registration </h1>
        <hr>
        <div class="newUser">
            <form id="new_user" method="post" action="">
                Your name:
                <input id="forename" type="text" autofocus="true" name="forename" required>
                <br/>

                Surname: 
                <input id="surname" type="text" name="surname">
                <br/>

                Student ID: 
                <input id="student_id" type="text" name="student_id" required>
                <br/>

                Password:
                <input id="password" type="password" name="password" required>
                <br/>

                <input type='hidden' name='register' value="register" />
                <input type="submit" value="Submit" id="submitForm" onclick="return validate()"/>
            </form>
        </div>
    </body>
</html>
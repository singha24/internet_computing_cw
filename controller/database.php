<?php

function getDb() {
    try {
        $servername = "localhost";
        $username = "root";
        $password = "root";
        $dbname = "astonbookstore";

        $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $conn;
    } catch (Exception $e) {
        die("Unable to connect to database");
    }
}

function logout() {
    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]
        );
    }
    session_destroy();
    header('Location: index.php');
}

function login() {

    $username = $_REQUEST['username'];
    $password = $_REQUEST['password'];
    $password = md5($password);
    $admin = "admin/console.php";
    $conn = getDb();

    $sql = $conn->prepare("SELECT student_id, name, account_type, balance, status FROM user WHERE student_id = :username AND password = :password LIMIT 1");
    $sql->bindValue(":username", $username);
    $sql->bindValue(":password", $password);
    $sql->execute();
    $result = $sql->setFetchMode(PDO::FETCH_ASSOC);

    $rows = $sql->fetchAll(PDO::FETCH_ASSOC);

    if ($result) {
        //if the query returns a value else return 1. 1 indicates login failure. 0 implies pending approval. 
        foreach ($rows as $row) {
            if ($row['account_type'] === "admin") {
                $_SESSION['session_id'] = $row['name'];
                $_SESSION['user_balance'] = $row['balance'];
                $_SESSION['account_type'] = 'admin';
                //header('Location: ' . $admin);
                return true;
            } else {
                $_SESSION['session_id'] = $row['name'];
                $_SESSION['user_balance'] = $row['balance'];
                $_SESSION['user'] = $row['student_id'];
            }
        }
    }else{
        echo "<script type='text/javascript'>alert('Failed to login. Please contact the site administrator.');</script>";
    }
}

function getCategories() {
    $conn = getDb(); // gets connection to MySQL

    $sql = $conn->prepare("SELECT * FROM category ORDER BY category ASC");
    $sql->execute(); // runs SQL statement
    $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
    $rows = $sql->fetchAll(); // holds array containing each row
    foreach ($rows as $row) {
        echo'<option value="' . $row['category_id'] . '">' . $row['category'] . '</option>';
    }
}

?>
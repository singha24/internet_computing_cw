<?php

function displayMultipleCategoryBook($bool) {
    $category_id = $_GET['category'];
    $query = "SELECT DISTINCT book.* FROM book, multiple_category WHERE multiple_category.category_id='$category_id' ORDER BY book.book_title ASC";
    if (!($bool === "true")) {
        displayAllBooks();
    } else {
        try {
            $conn = getDb();

            $sql = $conn->prepare($query);
            $sql->execute(); // runs SQL statement
            // set the resulting array to associative
            $rows = $sql->fetchAll(); // holds array containing each row
            echo '<div id="displayBooks">';
            echo '<ul class="products">';
            foreach ($rows as $row) {
                echo '<form method="post">';
                echo '<img src="http://placehold.it/350x150" height="120" width="110">' . '<br>';
                echo '<b>' . $row['book_title'] . '</b><br>';
                echo 'ISBN: ' . $row['isbn'] . '<br>';
                //echo 'Category: ' . $row['category'] . '<br>';
                echo '<b><em>Price: &#163;' . $row['price'] . '</em></b><br>';
                echo '<b><em>Stock: ' . $row['stock'] . '</em></b><br>';

                //Only allow user's who have logged in to add books ot cart, OR if admin disallow them from making purchase. 
                if (isset($_SESSION['session_id'])) {
                    if ($row['stock'] != 0) { //If stock = 0 disble the purchase option
                        echo 'Qty <input type="text" name="product_qty" value="1" size="3" />';
                        echo '<button>Add To Cart</button>';
                    }
                } else {
                    echo '<button disabled>Add To Cart</button>';
                }
                echo '<input type="hidden" name="book_title" value="' . $row['book_title'] . '" />';
                echo '<input type="hidden" name="product_price" value="' . $row['price'] . '" />';
                echo '<input type="hidden" name="product_code" value="' . $row['isbn'] . '" />';
                echo '<input type="hidden" name="type" value="add" />';
                echo "</form>";
            }
            echo '</ul>';
            echo '</div>';
        } catch (PDOException $e) {
            echo $e->getMessage();
            die();
        }
    }

    $conn = null;
}

function displayAllBooks() {
    $query = null;
    $isbnArray = [];
    $multiple_category = [];
    $final;
    $conn = getDb(); // gets connection to MySQL

    $query = "SELECT * FROM book";

    $sql = $conn->prepare($query);
    $sql->execute(); // runs SQL statement
    $rows = $sql->fetchAll();
    echo '<div id="displayBooks">';
    echo '<ul class="products">';
    foreach ($rows as $row) {
        echo '<form method="post">';
        echo '<img src="http://placehold.it/350x150" height="120" width="110">' . '<br>';
        echo '<b>' . $row['book_title'] . '</b><br>';
        echo 'ISBN: ' . $row['isbn'] . '<br>';
//echo 'Category: ' . $row['category'] . '<br>';
        echo '<b><em>Price: &#163;' . $row['price'] . '</em></b><br>';
        echo '<b><em>Stock: ' . $row['stock'] . '</em></b><br>';

//Only allow user's who have logged in to add books ot cart, OR if admin disallow them from making purchase.
        if (isset($_SESSION['session_id'])) {
            if ($row['stock'] != 0) { //If stock = 0 disble the purchase option
                echo 'Qty <input type="text" name="product_qty" value="1" size="3" />';
                echo '<button>Add To Cart</button>';
            }
        } else {
            echo '<button disabled>Add To Cart</button>';
        }
        echo '<input type="hidden" name="book_title" value="' . $row['book_title'] . '" />';
        echo '<input type="hidden" name="product_price" value="' . $row['price'] . '" />';
        echo '<input type="hidden" name="product_code" value="' . $row['isbn'] . '" />';
        echo '<input type="hidden" name="type" value="add" />';
        echo "</form>";
    }
    echo '</ul>';
    echo '</div>';
}

?>

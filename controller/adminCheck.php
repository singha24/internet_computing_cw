<?php
if (!isset($_SESSION)) {
    session_start();
}
/* 
 *This file is require in order to manage admin pages. 
 */
if (!(isset($_SESSION['account_type']) && $_SESSION['account_type'] == "admin")) {
    header('Location: ' . '../index.php');
}

?>
<!DOCTYPE html>
<?php
include '../controller/database.php';
if (isset($_REQUEST['function']) && $_REQUEST['function'] = "submit") {
    login();
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Aston Book Store | Login</title>
        <link rel="stylesheet" type="text/css" href="../css/login.css">
    </head>
    <body>

        <div class="login">
            <form method="post">
                <input type="text" autofocus="true" name="username" placeholder="Username" id="username">  
                <input type="password" name="password" placeholder="Password" id="password">  
                
                <input type='hidden' name='function' value="login" />
                <input type="submit" value="Sign In">
            </form>
        </div>
    </body>
</html>

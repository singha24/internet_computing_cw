<?php

/*
 * A php file to process an order.
 */
session_start();
include 'database.php';

if (isset($_SESSION['user'])) {
    $user_id = $_SESSION['user'];
} else {
    echo "You Must be logged in to perform this action.";
    header('Location: ../index.php');
    die();
}

$success = false;
$isbn = null;
$quantity = null;

$conn = getDb();
foreach ($_GET['item_code'] as $itm) {
    $isbn = $itm;
    foreach ($_GET['item_qty'] as $qty) {
        $quantity = $qty;

        $stmt = 'INSERT INTO purchase_history (isbn, quantity, student_id)
    VALUES (:isbn,:quantity,:user_id)';
        $sql = $conn->prepare($stmt);
        $sql->bindValue(":isbn", $isbn, PDO::PARAM_STR);
        $sql->bindValue(":quantity", $quantity, PDO::PARAM_INT);
        $sql->bindValue(":user_id", $user_id, PDO::PARAM_STR);

        //if the order has been placed then, empty the basket and then redirect to homepage.
        try {
            if ($sql->execute()) {
                $success = true;
            }
        } catch (Exception $e) {
            echo $e;
        }
    }
}

if ($success) {
    echo "<script type='text/javascript'>var r = confirm('Order placed successfully, please go to in-store to complete the order.'); "
    . "if (r == true) {window.location='../index.php';}else{window.location='../index.php';}</script>";
    unset($_SESSION['displayBooks']);
}else{
    echo "<script type='text/javascript'>var r = confirm('Failed to checkout, please contact shop administrator.'); "
    . "if (r == true) {window.location='../index.php';}else{window.location='../index.php';}</script>";
}
?>
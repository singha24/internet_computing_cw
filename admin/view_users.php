<!DOCTYPE html>
<?php
if (!isset($_SESSION)) {
    session_start();
}
require '../controller/adminCheck.php';
include '../DAO/UserDAO.php';
include 'console.php';
if (isset($_REQUEST['verify']) && $_REQUEST['verify'] = "Verify") {
    verify();
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>USERS</title>
    </head>
    <body>
        <h1>Current Users</h1>
        <hr>
        <div id="users">
            <table>
                <thead>
                    <tr> 
                        <th><a href="#">No.</a></th>
                        <th><a href="#">Student_id</a></th>
                        <th><a href="#">Name</a></th>
                        <th><a href="#">Balance</a></th>
                        <th><a href="#">Account Type</a></th>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    $conn = getDb(); // gets connection to MySQL

                    $sql = $conn->prepare("SELECT student_id, name, surname, balance, account_type FROM user WHERE status !='pending'");
                    $sql->execute(); // runs SQL statement
                    // set the resulting array to associative
                    $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
                    $rows = $sql->fetchAll(); // holds array containing each row
                    $counter = 1;
                    foreach ($rows as $row) {

                        echo "<tr>";
                        echo "<td>" . $counter . "</td>";
                        echo "<td  class='overflow-ellipsis'>" . $row['student_id'] . "</td>";
                        echo "<td class='overflow-ellipsis'> " . $row['name'] . ' ' . $row['surname'] . "</td>";
                        echo "<td  class='overflow-ellipsis'> " . $row['balance'] . "</td>";
                        echo "<td  class='overflow-ellipsis'> " . $row['account_type'] . "</td>";

                        $counter++;
                    }
                    ?>

                </tbody>
            </table>

        </div>
        <hr/>
        <br/>
        <div id="pending_users">
            <h1>Pending Registrations</h1>
            <table>
                <?php
                $conn = getDb(); // gets connection to MySQL

                $sql = $conn->prepare("SELECT * FROM user WHERE status='pending'");
                $sql->execute(); // runs SQL statement
                // set the resulting array to associative
                $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
                $rows = $sql->fetchAll(); // holds array containing each row
                if (count($rows) != 0) {
                    echo'<thead>';
                    echo'<tr>';
                    echo '<th><a href="#">No.</a></th>';
                    echo '<th><a href="#">Student_id</a></th>';
                    echo '<th><a href="#">Name</a></th>';
                    echo '<th><a href="#">Balance</a></th>';
                    echo '<th><a href="#">Account Type</a></th>';
                    echo '<th><a href="#">Status</a></th>';

                    echo'</tr>';
                    echo'</thead>';
                    echo'<tbody>';
                    $counter = 1;
                    foreach ($rows as $row) {

                        echo "<tr>";
                        echo "<td>" . $counter . "</td>";
                        echo "<td  class='overflow-ellipsis'>" . $row['student_id'] . "</td>";
                        echo "<td class='overflow-ellipsis'> " . $row['name'] . ' ' . $row['surname'] . "</td>";
                        echo "<td  class='overflow-ellipsis'> " . $row['balance'] . "</td>";
                        echo "<td  class='overflow-ellipsis'> " . $row['account_type'] . "</td>";
                        echo "<td  class='overflow-ellipsis'> " . $row['status'] . "</td>";
                        echo '<td>';
                        echo '<form method="post">';
                        echo '<input type="hidden" name="student_id" value="' . $row['student_id'] . '"/>';
                        echo '<input type="submit" name="verify" value="Verify" />';
                        echo '</td>';
                        echo '</tr>';
                        $counter++;
                    }
                    echo'</tbody>';
                }else{
                    echo '<th> No pending registrations </th>';
                }
                ?>
            </table>

        </div>
    </body>
</html>

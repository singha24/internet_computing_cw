<?php
if (!isset($_SESSION)) {
    session_start();
}
if (isset($_SESSION['account_type']) && $_SESSION['account_type'] == "admin") {
    echo '<html>';
    echo '<head>';
    echo '<meta charset="UTF-8">';
    echo '<title>Aston Book Store - Admin</title>';
    echo '<link rel="stylesheet" type="text/css" href="../css/console.css">';
    echo '</head>';
    echo '<body>';
    echo '<div id="nav">';
    echo '<ul>';
    echo '<li><a href="../index.php">Back to shop</a></li>';
    echo '<li><a href="newBook.php">Add New Book</a></li>';
    echo '<li><a href="topUpBalance.php">Top-up user balance</a></li>';
    echo '<li><a href="orders.php">View orders</a></li>';
    echo '<li><a href="viewStock.php">View Stock</a></li>';
    echo '<li><a href="view_users.php">View Users</a></li>';
    echo '</ul>';
    echo '<hr id="adminConsole"/>';
    echo '</div>';
    echo '</body>';
    echo '</html>';
}else{
    header('Location: ' . '../index.php');
}
?>
        

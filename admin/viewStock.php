<!DOCTYPE html>
<?php
if (!isset($_SESSION)) {
    session_start();
}
require '../controller/adminCheck.php';
include '../DAO/CurrentStockDAO.php';
include 'console.php';
if (isset($_REQUEST['remove']) && $_REQUEST['remove'] = "Remove Book") {
    removeBook();
}
if (isset($_REQUEST['add_stock']) && $_REQUEST['add_stock'] = "Add Stock") {
    if ($_POST['stock_val'] <= 0) {
        echo "<script type='text/javascript'>alert('Please enter a value greater than 0 to update stock');</script>";
    } else {
        addStock();
    }
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Stock Check</title>
        <link rel="stylesheet" type="text/css" href="../css/console.css">
    </head>
    <body>
        <h1>Stock</h1>
        <hr>
        <div id="currentStock">
            <table>
                <thead>
                    <tr> 
                        <th><a href="#">Index no.</a></th>
                        <th><a href="#">Book Title</a></th>
                        <th><a href="#">ISBN</a></th>
                        <th><a href="#">Price (£)</a></th>
                        <th><a href="#">Stock</a></th>

                    </tr>
                </thead>
                <tbody>
                    <?php
                    $rows = getStock();
                    $counter = 1;
                    //  var_dump($rows[0]); // dumps a whole row
                    foreach ($rows as $row) {

                        echo "<tr>";
                        echo "<td>" . $counter . "</td>";
                        echo "<td  class='overflow-ellipsis'>" . $row['book_title'] . "</td>";
                        echo "<td class='overflow-ellipsis'> " . $row['isbn'] . "</td>";
                        echo "<td class='overflow-ellipsis'> " . $row['price'] . "</td>";
                        echo "<td  class='overflow-ellipsis'> " . $row['stock'] . "</td>";

                        echo "<td>";
                        echo "<form method='post'><input type='hidden' name='book_id' value='" . $row['isbn'] . "' />"
                        . "<input type='submit' name='remove' value='Remove Book' /></form>"
                        . "</td>";
                        echo '<td>';
                        echo '<form method="post">';
                        echo '<input type="number" name="stock_val"/>';
                        echo '<input type="hidden" name="book_isbn" value="' . $row['isbn'] . '"/>';
                        echo '<input type="submit" name="add_stock" value="Add Stock" />';
                        echo '</form>';
                        echo '</td>';
                        echo "</tr>";
                        $counter++;
                    }
                    ?>

                </tbody>
            </table>

        </div>
    </body>
</html>

<!DOCTYPE html>
<?php
if (!isset($_SESSION)) {
    session_start();
}
require '../controller/adminCheck.php';
include '../DAO/TopUpBalanceDAO.php';
include 'console.php';
if (isset($_REQUEST['amount']) && $_REQUEST['amount'] = "amount") {
    if ($_POST['amount'] <= 0) {
        echo "<script type='text/javascript'>alert('Please enter a value greater than 0 to credit user');</script>";
    } else {
        topUp();
    }
}
if (isset($_REQUEST['reset']) && $_REQUEST['reset'] = "Reset") {
    unset($_SESSION['topup']);
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Top-up</title>
        <link rel="stylesheet" type="text/css" href="../css/topup.css">
    </head>
    <body>
        <h1> Top-up a students balance </h1>
        <!--<h3> Please note the STUDENT_ID you enter FIRST will be the one which gets modified. All subsequent Student-id's will be ignored. </h3>-->
        <hr>
        <div class="topup_student">
            <?php
            $request_amount = NULL;
            $user = NULL;
            $name = NULL;
            if (isset($_REQUEST['student_id']) && $_REQUEST['student_id'] = "student_id") {
                $request_amount = getStudent();
                $name = getUser($_POST['student_id']);
                if ($request_amount == false) {
                    unset($_SESSION['topup']);
                    echo "<script type='text/javascript'>alert('Unable to find specified user, please check your input and try again.');</script>";
                }
            } else {
                echo '<form id="student_check" method="post" action="">';
                echo 'Student ID:';
                echo '<input id="student_id" type="textarea" autofocus="true" name="student_id" required>';
                echo '<br/>';

                echo '<input type="hidden" value="topup" name="topup"/>';
                echo '<input type="submit" value="Search" id="submitForm"/>';
                echo '</form>';
                echo '<hr/>';
            }
            //echo '<br/>';
            
            if ($request_amount != false) {
                echo '<em>Amount in ' . strtoupper($name[0]['name']) . '\'s' . ' balance: £' . $request_amount . '</em>';
            }

            if (isset($_REQUEST['student_id']) && $_REQUEST['student_id'] = "student_id" && $request_amount != false) {
                echo '<form id="topup" method="post" action="">';
                echo '<br/>';
                echo 'Amount to Credit (in GBP Sterling): ';
                echo '<input id="credit" type="number" name="amount" value="amount" required>';
                echo '<br/>';
                echo '<input type="submit" value="Submit" id="submitForm"/>';
                echo '</form>';
                echo '<form id="reset" method="post" action="">';
                echo '<input type="submit" value="Reset" name="reset"/>';
                echo '</form>';
            }
            ?>
        </div>

        <?php
        // put your code here
        ?>
    </body>
</html>

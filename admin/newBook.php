<!DOCTYPE html>
<?php
if (!isset($_SESSION)) {
    session_start();
}
require '../controller/adminCheck.php';
include '../DAO/AddBookDAO.php';
include 'console.php';
require '../controller/adminCheck.php';
if (isset($_REQUEST['function']) && $_REQUEST['function'] = "submit") {
    if (!addBook()) {
        echo "<script type='text/javascript'>alert('Failed to add book!\n 1) Please check that the book doesnt already exist.\n"
        . "2) Check that the value for Price is actually a number.\n"
                . "3) Check that you have submitted a legitimate value for stock.);</script>";
    }
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>New Book</title>
        <link rel="stylesheet" type="text/css" href="../css/newbook.css">
        <script type="text/javascript">
            function validate() {
                var errors = "";
                var fail = false;

                var book_title = document.forms["new_book"]["book_tittle"].value;
                var stock_val = document.forms["new_book"]["stock"].value;
                var price = document.forms["new_book"]["price"].value;
                var isbn = document.forms["new_book"]["book_isbn"].value;

                //if ((isNaN(price))) {
                    //errors += "Price must be a numerical value";
                    //fail = true;
                //}
                if (book_title == null || book_title == "") {
                    errors += "Book title cannot be left blank.\n";
                    fail = true;
                }

                var isbn_length = isbn.length;

                if (isbn == null || isbn == "") {
                    errors += "ISBN cannot be left blank.\n";
                    fail = true;
                }

                if (fail) {
                    alert(errors);
                }

                if (stock_val <= 0 || price <= 0) {
                    errors += "Please enter a value for Stock/Price above 0.\n";
                    fail = true;
                }

                if (isbn_length > 14) {
                    errors += "Invalid ISBN value.\n";
                    fail = true;
                } else {
                    return true;
                }

                if (!(fail)) {
                    return true;
                } else {
                    return false;
                }
            }
            
            function disabled(){
                alert('NOTICE\n This feature is still under development.');
                return false;
            }
        </script>
    </head>
    <body>
        <h1> Create new book entry </h1>
        <hr>
        <div class="newBook">
            <form id="new_book" method="post" action="">
                Book Title:
                <input id="book_tittle" type="textarea" autofocus="true" name="book_title" required>
                <br/>

                ISBN: 
                <input id="book_isbn" type="text" name="isbn" maxlength="14" required>
                <br/>

                Category:
                <br/>
                <select id="categories" name="category_id[]" multiple>
                    <?php getCategories(); ?>
                </select>
                <br/>

                Price: 
                <b>£</b><input id="book_price" type="text" name="price" required>
                <br/>

                Stock:
                <input id="book_stock" type="number" name="stock" required><b>units</b>
                <br/>

                Image: 
                <input id="book_image" type="textarea" name="image" onmousedown="disabled()" disabled="true">
                <br/>

                <input type='hidden' name='function' value="addBook" />
                <input type="submit" value="Submit" id="submitForm" onclick="return validate()"/>
            </form>
        </div>
    </body>
</html>

<!DOCTYPE html>
<?php
if (!isset($_SESSION)) {
    session_start();
}
require '../controller/adminCheck.php';
include '../DAO/OrdersDAO.php';
include 'console.php';
if (isset($_REQUEST['remove']) && $_REQUEST['remove'] = "Complete order") {
    completeOrder();
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Incoming orders</title>
    </head>
    <body>
        <h1>Orders</h1>
        <hr>
        <div id="currentStock">
            <table>
                <?php
                $rows = viewOrders();
                if ($rows != NULL) {
                    echo '<thead>';
                    echo '<tr> ';
                    echo '<th><a href="#">Purchase ID</a></th>';
                    echo '<th><a href="#">Order Number</a></th>';
                    echo '<th><a href="#">Book Title</a></th>';
                    echo '<th><a href="#">Quantity</a></th>';
                    echo '<th><a href="#">TOTAL</a></th>';
                    echo '<th><a href="#">Customer Name</a></th>';
                    echo '<th><a href="#">Customer ID</a></th>';
                    echo '<th><a href="#">Customer Balance (£)</a></th>';

                    echo '</tr>';
                    echo '</thead>';
                    echo '<tbody>';


                    $counter = 1;
                    //var_dump($rows['isbn']); // dumps a whole row
                    foreach ($rows as $row) {
                        echo "<tr>";
                        echo "<td>" . $counter . "</td>";
                        echo "<td  class='overflow-ellipsis'>" . $row['purchase_id'] . "</td>";
                        echo "<td  class='overflow-ellipsis'>" . $row['book_title'] . "</td>";
                        echo "<td class='overflow-ellipsis'> " . $row['quantity'] . "</td>";
                        echo "<td class='overflow-ellipsis'> " . $row['quantity'] * $row['price'] . "</td>";
                        echo "<td  class='overflow-ellipsis'> " . $row['name'] . "</td>";
                        echo "<td  class='overflow-ellipsis'> " . $row['student_id'] . "</td>";
                        echo "<td  class='overflow-ellipsis'> " . $row['balance'] . "</td>";
                        //only allow if stock is not = 0
                        if (!($row['stock']) <= 0) {
                            if ($row['balance'] < $row['price'] * $row['quantity']) {
                                echo '<td>';
                                echo '<a href="topUpBalance.php">Insufficient funds to complete order</a>';
                                echo '</td>';
                            } else {
                                if (!($row['quantity'] > $row['stock'])) {
                                    echo '<td>';
                                    echo '<form method="post">';
                                    //var_dump($row['isbn']);
                                    echo '<input type="submit" name="remove" value="Complete order"/>';
                                    echo '<input type="hidden" name="purchase_id" value="' . $row['purchase_id'] . '"/>';
                                    echo '<input type="hidden" name="customer" value="' . $row['student_id'] . '"/>';
                                    echo '<input type="hidden" name="book" value="' . $row['isbn'] . '"/>';
                                    echo '<input type="hidden" name="total" value="' . $row['price'] * $row['quantity'] . '"/>';
                                    echo '<input type="hidden" name="qty" value="' . $row['quantity'] . '"/>';
                                    echo '</form>';
                                    echo '</td>';
                                } else {
                                    echo '<td>';
                                    echo 'Unable to proceed';
                                    echo '</td>';
                                }
                            }
                        } else {
                            echo '<td>';
                            echo 'Out Of Stock';
                            echo '</td>';
                        }


                        $counter++;
                    }
                } else {
                    echo '<th> No incoming orders </th>';
                }

                echo '</tbody>';
                ?>
            </table>

        </div>

    </body>
</html>

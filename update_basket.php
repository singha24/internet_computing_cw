<?php
//if session doesnt exist start one.
if (!isset($_SESSION)) {
    session_start();
}
include_once("controller/database.php");

//process add/update basket request
if (isset($_POST["type"]) && $_POST["type"] == 'add') {

    $book_isbn = $_POST["product_code"]; //ISBN
    $qty = $_POST["product_qty"]; //Quantity of items

    $success = true;

    $conn = getDb();
    $sql = $conn->prepare("SELECT book_title, price, stock FROM book WHERE isbn=:product_code LIMIT 1");
    $sql->bindValue(":product_code", $book_isbn);
    $sql->execute();
    $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
    $rows = $sql->fetchAll();

    foreach ($rows as $row) {
    if ($qty > $row['stock']) {
            unset($_SESSION['displayBooks']);
            $success = false;
        }
        if ($success == true) {
            //if exists add the book to the array. 
            if ($result) {
                $new_product = array(array('book_title' => $row['book_title'], 'isbn' => $book_isbn, 'qty' => $qty, 'price' => $row['price']));
                //if books have been added to the basket
                if (isset($_SESSION["displayBooks"])) {
                    $found = false;

                    foreach ($_SESSION["displayBooks"] as $book) {

                        if ($book["isbn"] == $book_isbn) {
                            $product[] = array('book_title' => $book["book_title"], 'isbn' => $book["isbn"], 'qty' => $qty, 'price' => $book["price"]);
                            $found = true;
                        } else {
                            $product[] = array('book_title' => $book["book_title"], 'isbn' => $book["isbn"], 'qty' => $book["qty"], 'price' => $book["price"]);
                        }
                    }

                    if ($found == false) {
                        $_SESSION["displayBooks"] = array_merge($product, $new_product);
                    } else {
                        $_SESSION["displayBooks"] = $product;
                    }
                } else {
                    $_SESSION["displayBooks"] = $new_product;
                }
            }

            if ($success == false) {
                echo "<script type='text/javascript'>var r = confirm('You have specified a value which is greater than the stock.');</script>";
            } else {
                header('Location: index.php');
            }
        }
    }
}

//Process remove request
if (isset($_GET["remove"])) {
    $book_isbn = $_GET["remove"]; 
    $return_url = $_GET['return_url'];

    foreach ($_SESSION["displayBooks"] as $book) {
        if ($book["isbn"] != $book_isbn) {
            $product[] = array('book_title' => $book["book_title"], 'isbn' => $book["isbn"], 'qty' => $book["qty"], 'price' => $book["price"]);
        }

        $_SESSION["displayBooks"] = $product;
        /* echo '<pre>';
          var_dump($cart_itm);
          echo '</pre>'; */
    }
    
    header('Location: index.php');
}
?>
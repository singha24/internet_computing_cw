<?php

include '../controller/database.php';
require '../controller/adminCheck.php';
/*
 * A Database access object to to add books
 */

function addBook() {
    $conn = getDb();

    $isbn = $_REQUEST['isbn'];
    $categorys = $_REQUEST['category_id'];

    //used to convert catery id to return actual category
    /*$category_name = [];
    foreach ($categorys as $cat) {
        $category_name[] = getCategoryID($cat);
    }*/

    $price = $_REQUEST['price'];
    $stock = $_REQUEST['stock'];
    
    if($price <= 0 || $stock <= 0){
        return false;
        die();
    }
    
    $image = "imgs/" . $isbn;
    $bookTitle = $conn->quote($_REQUEST['book_title']);



    $sql = $conn->prepare("INSERT INTO book (isbn, price, stock,image,book_title)
    VALUES (:isbn,:price,:stock,:image,:bookTitle)");
    $sql->bindValue(":isbn", $isbn);
    $sql->bindValue(":price", $price);
    $sql->bindValue(":stock", $stock);
    $sql->bindValue(":image", $image);
    $sql->bindValue(":bookTitle", $bookTitle);
    
    try {
        if ($sql->execute()) {
            foreach ($categorys as $category){
                //var_dump($category);
                $sql_two = $conn->prepare("INSERT INTO multiple_category (isbn, category_id) VALUES (:isbn, :category)");
                $sql_two->bindValue(":isbn", $isbn);
                $sql_two->bindValue(":category", $category);
                if($sql_two->execute()){
                    echo "<script type='text/javascript'>alert('Success!');</script>";
                    return true;
                }
            }
        }
    } catch (Exception $e) {
        echo "<script type='text/javascript'>alert('Unable to complete request. \n Error: " . $e . "');</script>";
        //die();
        return false;
    }
}

function getCategoryID($cat) {
    $conn = getDb();
    $categorys = [];
    foreach ($cat as $category) {
        $sql = $conn->prepare("SELECT category FROM category WHERE category_id=:category");
        $sql->bindValue(":category", $category);
        $sql->execute(); // runs SQL statement
        $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
        $rows = $sql->fetchAll();
        if ($result) {
            foreach ($rows as $row) {
                $categorys[] = $row['category'];
            }
        } else {
            echo "<script type='text/javascript'>alert('Failed to fetch categories.');</script>";
        }
    }
    return $categorys;
}

?>
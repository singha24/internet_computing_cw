<?php

include '../controller/database.php';
require '../controller/adminCheck.php';
/*
 * A databse Access object for topping up credit.
 */

function topUp() {
    if (isset($_REQUEST['amount']) && $_POST['amount'] != NULL) {
        $conn = getDb();
        $amount = $_POST['amount'];
        $user = null;
        if (isset($_SESSION['topup'])) {
            $user = $_SESSION['topup'][0];
        }

        $sql = $conn->prepare("UPDATE user SET balance = balance + :amount WHERE student_id =:user");
        $sql->bindValue(":amount", $amount);
        $sql->bindValue(":user", $user);
        if ($sql->execute()) {
            echo "<script type='text/javascript'>alert('Success!');</script>";
        } else {
            echo "<script type='text/javascript'>alert('Failed to update balance');</script>";
        }
        unset($_SESSION['topup']); // = NULL;
    }
}

function getUser($user) {
    $conn = getDb();
    $sql = $conn->prepare("SELECT * FROM user WHERE student_id=:user");
    $sql->bindValue(":user", $user);
    $sql->execute();
    return $rows = $sql->fetchAll();
}

function getStudent() {

    if (isset($_REQUEST['topup']) && $_REQUEST['topup'] = "topup") {
        if (isset($_REQUEST['topup'])) {
            $user = $_POST['student_id'];
            $_SESSION['topup'][] = $user;
        }
        try {
            $conn = getDb();

            $sql = $conn->prepare("SELECT balance FROM user WHERE student_id=:user LIMIT 1");
            $sql->bindValue(":user", $user);
            $sql->execute(); // runs SQL statement
            $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
            $rows = $sql->fetchAll();
            if ($rows == NULL) {
                return false;
            } else {
                foreach ($rows as $row) {
                    return $row['balance'];
                }
            }
        } catch (Exception $e) {
            echo "<script type='text/javascript'>alert('Unable to find Student, please enter their  STUDENT_ID');</script>";
        }
    } else {
        echo "<script type='text/javascript'>alert('Unable to perform desired action, please try again.');</script>";
    }
}

?>

<?php

include '../controller/database.php';
require '../controller/adminCheck.php';
/*
 * Databse access object for handling Orders.
 */

function completeOrder() {

    $user = $_POST['customer'];
    $isbn = $_POST['book'];
    $total = $_POST['total'];
    $qty = $_POST['qty'];
    if ($qty > checkStockVal($qty)) {
        echo '<td>';
        echo 'Not enough in stock';
        echo '</td>';
    }
    $id = $_POST['purchase_id'];

    $conn = getDb();

    $sql = $conn->prepare("UPDATE book SET book.stock = stock - :qty WHERE book.isbn = :isbn;"
            . "UPDATE user SET user.balance = user.balance - :total WHERE user.student_id = :user;"
            . "DELETE FROM purchase_history WHERE purchase_id=:id;");
    $sql->bindValue(":qty", $qty);
    $sql->bindValue(":isbn", $isbn);
    $sql->bindValue(":total", $total);
    $sql->bindValue(":user", $user);
    $sql->bindValue(":id", $id);
    if ($sql->execute()) {
        echo "Transaction completed successfully";
        //var_dump($_POST);
        return true;
    } else {
        echo "Error";
        return false;
    }
}

function checkStockVal($isbn) {
    $conn = getDb(); // gets connection to db

    $sql = $conn->prepare("SELECT stock FROM book WHERE isbn=:isbn LIMIT 1");
    $sql->bindValue(":isbn", $isbn);
    $sql->execute(); // runs SQL statement
    // set the resulting array to associative
    $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
    $rows = $sql->fetchAll(); // holds array containing each row
    return $rows;
}

function viewOrders() {
    $conn = getDb(); // gets connection to db

    $sql = $conn->prepare("SELECT * FROM book, purchase_history, user WHERE purchase_history.isbn=book.isbn AND purchase_history.student_id=user.student_id");
    $sql->execute(); // runs SQL statement
    // set the resulting array to associative
    $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
    $rows = $sql->fetchAll(); // holds array containing each row
    return $rows;
}

?>

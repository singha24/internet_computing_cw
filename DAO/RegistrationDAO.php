<?php

/*
 * Status can either be approved or pending.
 * On registration the status will be pending. The admin must authenticate user.
 */
include 'controller/database.php';
define("MAX_LENGTH", 10);

function register() {
    $conn = getDb();

    $surname = " ";
    $status = "pending"; //all new user's will have the pending status applied to their account, It is the job of the admin to change this. 
    $balence = 1;
    $account_type = "student";
    $forename = $_REQUEST['forename'];
    $surname = $_REQUEST['surname'];
    $student_id = $_REQUEST['student_id'];
    $password = $_REQUEST['password'];
    $password = md5($password);
    
    //var_dump($_POST);

    ///var_dump($password);
    try {
        $sql = $conn->prepare("SELECT * FROM user WHERE student_id=:student_id LIMIT 1");
        $sql->bindValue(":student_id", $student_id);
        $sql->execute(); // runs SQL statement
        // set the resulting array to associative
        $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
        $rows = $sql->fetchAll(); // holds array containing each row
        if ($rows != null) {
            echo "<script type='text/javascript'>alert('A User with the same student id already exists.');</script>";
        } else {
            $sql = $conn->prepare("INSERT INTO user (student_id, name, surname, balance, account_type, password, status)"
                    . "VALUES('$student_id', '$forename', '$surname', $balence, '$account_type', '$password', '$status')");
            $sql->bindValue(":studnet_id", $student_id);
            $sql->bindValue(":forename", $forename);
            $sql->bindValue(":surname", $surname);
            $sql->bindValue(":balence", $balence);
            $sql->bindValue(":account_type", $account_type);
            $sql->bindValue(":password", $password);
            $sql->bindValue(":status", $status);
            if ($sql->execute()) {
                unset($_POST);
                //echo "<script type='text/javascript'>alert('Success!\n Please contact the administrator to approve your account.');</script>";
                header('Location: index.php');
            }else{
                return false;
            }
        }
    } catch (Exception $e) {
        $e->getMessage();
    }
}

function generateHashWithSalt($password) {
    $intermediateSalt = md5(uniqid(rand(), true));
    $salt = substr($intermediateSalt, 0, MAX_LENGTH);
    return hash("sha256", $password . $salt);
}

?>
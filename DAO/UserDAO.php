<?php
include_once '../controller/database.php';
require '../controller/adminCheck.php';
/*
 * Database access object to handle requests for user data.
 */

function verify() {
    $student_id = $_POST['student_id'];

    $conn = getDb();

    $sql = $conn->prepare("UPDATE user SET status = 'approved' WHERE student_id =:student_id");
    $sql->bindValue(":student_id", $student_id);
    try {
        $sql->execute();
    } catch (Exception $e) {
        echo "<script type='text/javascript'>alert('Unable to complete request" . $e . "');</script>";
        die();
    }
}

?>
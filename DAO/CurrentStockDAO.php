<?php

include_once '../controller/database.php';
require '../controller/adminCheck.php';
/*
 * A Databse access object which returns current stock of all books.
 */

function getStock() {
    $conn = getDb(); // gets connection to MySQL

    $sql = $conn->prepare("SELECT * FROM book ORDER BY book_title ASC");
    $sql->execute(); // runs SQL statement
    // set the resulting array to associative
    $result = $sql->setFetchMode(PDO::FETCH_ASSOC);
    $rows = $sql->fetchAll(); // holds array containing each row
    return $rows;
}

function removeBook() {
    $isbn = $_POST['book_id'];
    
    $conn = getDb();

    $sql = $conn->prepare("DELETE FROM book WHERE isbn = :isbn");
    $sql->bindValue(":isbn", $isbn);
    try {
        $sql->execute();
    } catch (Exception $e) {
        echo "<script type='text/javascript'>alert('Unable to complete request". $e ."');</script>";
        die();
    }
    
}

function addStock() {
    $isbn = $_POST['book_isbn'];
    $stock_val = $_POST['stock_val'];
    
    $conn = getDb();

    $sql = $conn->prepare("UPDATE book SET stock = stock + :stock_val WHERE isbn =:isbn");
    $sql->bindValue(":stock_val", $stock_val);
    $sql->bindValue(":isbn", $isbn);
    try {
        $sql->execute();
    } catch (Exception $e) {
        echo "<script type='text/javascript'>alert('Unable to complete request". $e ."');</script>";
        die();
    }
}

?>

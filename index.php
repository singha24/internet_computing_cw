<!DOCTYPE html>
<!--
Home page
-->
<?php
session_start(); //start new session to store information
include 'controller/database.php';
include 'update_basket.php';
include 'controller/displayBooks.php';
if (isset($_REQUEST['login']) && $_REQUEST['login'] = "login") {
    login();
}
if (isset($_REQUEST['register']) && $_REQUEST['register'] = "register") {
    header('Location: register.php');
}
if (isset($_SESSION['account_type']) && $_SESSION['account_type'] == "admin") {
    include 'controller/admin_session.php';
}

if (isset($_REQUEST['logout']) && $_REQUEST['logout'] = "logout") {
    logout();
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Aston Book Store</title>
        <link rel="stylesheet" type="text/css" href="css/index.css">
        <link rel="stylesheet" type="text/css" href="css/login.css">
        <link rel="stylesheet" type="text/css" href="css/basket.css">
    </head>
    <body>
        <div class="login">
            <?php
            if ($_SESSION == NULL) {
                echo '<div class="login/register">';

                echo '<div class="login">';
                echo '<form id="login_form" method="post">';
                echo '<input type="text" autofocus="true" name="username" placeholder="username" id="username">';
                echo '<input type="password" name="password" placeholder="Password" id="password">';
                echo '<input type="hidden" name="login" value="login" />';
                echo '<button id="login_submit" type="submit">Sign in</button>';
                echo '</form>';
                echo '</div>';
                echo '<br/>';
                //Register Form
                echo '<div class="register">';
                echo '<form id="register" method="get">';
                echo '<input type="hidden" name="register" value="register" />';
                echo '<button id="register_button" type="submit">Register</button>';
                echo '</form>';
                echo '</div>';

                echo '</div>';
            } else {
                echo '<form id="logout" method="post">';
                echo '<input type="hidden" name="logout" value="logout" />';
                echo '<button id="logout_submit" type="submit">Logout</button>';
                echo '</form>';
                //disable the checkout feature for admin
                if (isset($_SESSION['account_type']) != 'admin') {
                    echo '<div id="checkout">';
                    echo '<a href="view_cart.php">Check out</a>';
                    echo '</div>';
                }
            }
            ?>
        </div>
        <div class="header">
            <p>Aston Book Store</p>
            <?php
            if (isset($_SESSION['session_id'])) {
                echo '<div id="motd">';
                echo 'Welcome back ' . strtoupper($_SESSION['session_id']) . '!';
                echo '<br/>';
                echo '&#163;' . $_SESSION['user_balance'] . ' Remaining in your account.';
                echo '</div>';
            }
            ?>
        </div>
        <div class="filters">
            <div class="categorys">
                <?php
                echo '<form method="get" id="get_category">';
                echo '<select name="category">';
                getCategories();
                echo '</select>';
                echo '<input type="hidden" name="request_category" value="true"/>';
                echo '<input type="submit" value="Go" />';
                echo '<input type="submit" value="Reset" name="reset" />';
                echo '</form>';
                ?>
            </div>

        </div>
        <br/>
        <hr id="cat"/>
        <div>
            <?php
            $bool = "false";
            if (isset($_REQUEST['category']) && $_REQUEST['category'] != NULL) {

                if (isset($_REQUEST['request_category']) && $_REQUEST['request_category'] = "true") {
                    $bool = "true";
                }

                if (isset($_REQUEST['reset']) && $_REQUEST['reset'] = "Reset") {
                    $bool = "false";
                }

                displayMultipleCategoryBook($bool);
            } else {
                displayAllBooks();
            }
            ?>
        </div>
    </body>
</html>
